"""
Interact with Gym environments using the keyboard
An adapter object is defined for each environment to map keyboard commands to actions and extract observations as pixels.
"""

import sys
import ctypes
import argparse
import abc
import time
import os
import numpy as np
import pickle
import retro
import wandb
import shutil
import pyglet
import pathlib
import gzip
import zipfile
import glob as glob
from pyglet import gl
from pyglet.window import key as keycodes


parser = argparse.ArgumentParser(description="Human Play arguments")
parser.add_argument('--romartifact', required=True)

args = parser.parse_args()

args.retroname = args.romartifact.split(':')[0]

# wandb setup, execute first to log into wandb
run = wandb.init(
    project="turbocharger",
    config=args,
    job_type='human-play')

# get the rom and put it into retro's dir
rom = run.use_artifact(args.romartifact)
rom_dir = rom.download()

retro_location = retro.__file__
retro_path = pathlib.Path(retro.__file__).parent
print(retro_path)

# Move the rom and associated metadata jsons to the right paths
shutil.copyfile(rom_dir + "/rom.sfc", retro_path/"data/stable"/args.retroname/"rom.sfc")
shutil.copyfile(rom_dir + "/data.json", retro_path/"data/stable"/args.retroname/"data.json")
shutil.copyfile(rom_dir + "/metadata.json", retro_path/"data/stable"/args.retroname/"metadata.json")

# Logging lists for tiles
tiles_touched = dict()
buttons_pressed = list()
unique_tiles_ot = list()

class Interactive(abc.ABC):
    """
    Base class for making gym environments interactive for human use
    """
    def __init__(self, env, sync=True, tps=60, aspect_ratio=None):
        obs = env.reset()
        self._image = self.get_image(obs, env)
        assert len(self._image.shape) == 3 and self._image.shape[2] == 3, 'must be an RGB image'
        image_height, image_width = self._image.shape[:2]

        if aspect_ratio is None:
            aspect_ratio = image_width / image_height

        # guess a screen size that doesn't distort the image too much but also is not tiny or huge
        display = pyglet.canvas.get_display()
        screen = display.get_default_screen()
        max_win_width = screen.width * 0.9
        max_win_height = screen.height * 0.9
        win_width = image_width
        win_height = int(win_width / aspect_ratio)

        while win_width > max_win_width or win_height > max_win_height:
            win_width //= 2
            win_height //= 2
        while win_width < max_win_width / 2 and win_height < max_win_height / 2:
            win_width *= 2
            win_height *= 2

        win = pyglet.window.Window(width=win_width, height=win_height)

        self._key_handler = pyglet.window.key.KeyStateHandler()
        win.push_handlers(self._key_handler)
        win.on_close = self._on_close

        gl.glEnable(gl.GL_TEXTURE_2D)
        self._texture_id = gl.GLuint(0)
        gl.glGenTextures(1, ctypes.byref(self._texture_id))
        gl.glBindTexture(gl.GL_TEXTURE_2D, self._texture_id)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_NEAREST)
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_RGBA8, image_width, image_height, 0, gl.GL_RGB, gl.GL_UNSIGNED_BYTE, None)

        self._env = env
        self._win = win
        self._current_buttons = None

        # self._render_human = render_human
        self._key_previous_states = {}

        self._steps = 0
        self._episode_steps = 0
        self._episode_returns = 0
        self._prev_episode_returns = 0

        self._tps = tps
        self._sync = sync
        self._current_time = 0
        self._sim_time = 0
        self._max_sim_frames_per_update = 4

    def _update(self, dt):
        # cap the number of frames rendered so we don't just spend forever trying to catch up on frames
        # if rendering is slow
        max_dt = self._max_sim_frames_per_update / self._tps
        if dt > max_dt:
            dt = max_dt

        # catch up the simulation to the current time
        self._current_time += dt

        while self._sim_time < self._current_time:
            self._sim_time += 1 / self._tps

            keys_clicked = set()
            keys_pressed = set()
            for key_code, pressed in self._key_handler.items():
                if pressed:
                    keys_pressed.add(key_code)

                if not self._key_previous_states.get(key_code, False) and pressed:
                    keys_clicked.add(key_code)
                self._key_previous_states[key_code] = pressed

            # assume that for async environments, we just want to repeat keys for as long as they are held
            inputs = keys_pressed
            if self._sync:
                inputs = keys_clicked

            keys = []
            for keycode in inputs:
                for name in dir(keycodes):
                    if getattr(keycodes, name) == keycode:
                        keys.append(name)

            act = self.keys_to_act(keys)

            if not self._sync or act is not None:
                # print(f"act: {act}")
                obs, rew, done, _info = self._env.step(act)
                self._current_buttons = act
                self._image = self.get_image(obs, self._env)
                self._episode_returns += rew
                self._steps += 1
                self._episode_steps += 1
                np.set_printoptions(precision=2)

                # wandb log all the stuff out of the emulator
                tile = (_info['level'], _info['x_position']//32, _info['y_position']//32)
                tiles_touched[tile] = 1
                unique_tiles_ot.append(len(tiles_touched))
                buttons_pressed.append(act)
                
                # tiles_touched_ot.append(len(tiles_touched))
                if self._steps % 30 == 0:
                    wandb.log(_info, step=self._steps)
                    wandb.log({'tiles_touched': len(tiles_touched)})
            
                if done:
                    self._env.reset()
                    self._episode_steps = 0
                    self._episode_returns = 0
                    self._prev_episode_returns = 0
            # Exit only if the env has been stepped and esc is pressed
            if keycodes.P in keys_clicked:
                self._env.reset()
#                 self._env.em.set_state(retro.State.DEFAULT)
            
            if keycodes.ESCAPE in keys_pressed:
                self._on_close()

    def _draw(self):
        gl.glBindTexture(gl.GL_TEXTURE_2D, self._texture_id)
        video_buffer = ctypes.cast(self._image.tobytes(), ctypes.POINTER(ctypes.c_short))
        gl.glTexSubImage2D(gl.GL_TEXTURE_2D, 0, 0, 0, self._image.shape[1], self._image.shape[0], gl.GL_RGB, gl.GL_UNSIGNED_BYTE, video_buffer)

        x = 0
        y = 0
        w = self._win.width
        h = self._win.height

        pyglet.graphics.draw(
            4,
            pyglet.gl.GL_QUADS,
            ('v2f', [x, y, x + w, y, x + w, y + h, x, y + h]),
            ('t2f', [0, 1, 1, 1, 1, 0, 0, 0]),
        )

    def _on_close(self):
        print("Writing buttons to disk")
        artifact = wandb.Artifact(args.retroname + "-human", type='human-data')
        with open("humanplay_tiles_ot.npy", "wb") as f:
            np.save(f, np.array(unique_tiles_ot))    
        artifact.add_file('humanplay_tiles_ot.npy', 'humanplay_tiles_ot.npy')
        self._env.stop_record()
#         Add the bk2 after the env is closed so the bk2 is saved out
        shutil.copyfile(retro_path/"data/stable"/args.retroname/"boot.state", "./core.bin") # this moves the boot state to the working dir and renames it as core.bin
        
        movies = glob.glob(f"{args.retroname}*.bk2")
        print("Found movies")
        print(movies)
        for m in movies:
            zip = zipfile.ZipFile(m,'a')
            zip.write("./core.bin")
            zip.close()
            artifact.add_file(m)
            os.remove(m)
        wandb.log_artifact(artifact)
        print("About to exit")
        sys.exit(0)

    @abc.abstractmethod
    def get_image(self, obs, venv):
        """
        Given an observation and the Env object, return an rgb array to display to the user
        """
        pass

    @abc.abstractmethod
    def keys_to_act(self, keys):
        """
        Given a list of keys that the user has input, produce a gym action to pass to the environment
        For sync environments, keys is a list of keys that have been pressed since the last step
        For async environments, keys is a list of keys currently held down
        """
        pass

    def run(self):
        """
        Run the interactive window until the user quits
        """
        # pyglet.app.run() has issues like https://bitbucket.org/pyglet/pyglet/issues/199/attempting-to-resize-or-close-pyglet
        # and also involves inverting your code to run inside the pyglet framework
        # avoid both by using a while loop
        prev_frame_time = time.time()
        while True:
            self._win.switch_to()
            self._win.dispatch_events()
            now = time.time()
            self._update(now - prev_frame_time)
            prev_frame_time = now
            self._draw()
            self._win.flip()


    def run_record(self):
        """
        Run the interactive window, but also record screenshots and RAMs and states
        The intervals are how many frames to elapse before doing a specific capture
        """
        # pyglet.app.run() has issues like https://bitbucket.org/pyglet/pyglet/issues/199/attempting-to-resize-or-close-pyglet
        # and also involves inverting your code to run inside the pyglet framework
        # avoid both by using a while loop
        prev_frame_time = time.time()
        while True:
            self._win.switch_to()
            self._win.dispatch_events()
            now = time.time()
            self._update(now - prev_frame_time)
            prev_frame_time = now
            self._draw()
            self._win.flip()

class RetroInteractive(Interactive):
    """
    Interactive setup for retro games
    """

    def __init__(self, game, state, scenario):
        boot_env = retro.make(game=game)
        boot_state = boot_env.em.get_state()
        with open("boot.state", "wb") as f:
            f.write(gzip.compress(boot_state))
        boot_env.close()
#         When the movie is made, you will need to get this boot_env gzip state file into the bk2 file, which is just a zip file
        shutil.copyfile("boot.state", retro_path/"data/stable"/args.retroname/"boot.state")
        print("Starting env")
        env = retro.make(game=game, record=f".", state=retro.State.DEFAULT)
        env.initial_state = boot_state
        self._buttons = env.buttons
        super().__init__(env=env, sync=False, tps=60, aspect_ratio=4/3)

    def get_image(self, _obs, env):
        return env.render(mode='rgb_array')

    def keys_to_act(self, keys):
        inputs = {
            None: False,

            'BUTTON': 'Z' in keys,
            'A': 'Z' in keys,
            'B': 'X' in keys,

            'C': 'C' in keys,
            'X': 'A' in keys,
            'Y': 'S' in keys,
            'Z': 'D' in keys,

            'L': 'Q' in keys,
            'R': 'W' in keys,

            'UP': 'UP' in keys,
            'DOWN': 'DOWN' in keys,
            'LEFT': 'LEFT' in keys,
            'RIGHT': 'RIGHT' in keys,

            'MODE': 'TAB' in keys,
            'SELECT': 'TAB' in keys,
            'RESET': 'ENTER' in keys,
            'START': 'ENTER' in keys,
        }
        return [inputs[b] for b in self._buttons]


def main():
    print("Run interactive")
    ri = RetroInteractive(game=args.retroname, state=retro.State.NONE, scenario=None)
    ri.run_record()


if __name__ == '__main__':
    main()
