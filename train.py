import retro
import argparse
import wandb
import pathlib
import numpy as np
import imageio
import pickle
import os
import shutil
import glob
import tensorflow as tf
from wandb.keras import WandbCallback

parser = argparse.ArgumentParser(description="Training arguments")
parser.add_argument('--romartifact', required=True)
parser.add_argument('--humantrainartifact', required=True)
parser.add_argument('--humantestartifact', required=True)
parser.add_argument('--cap_freq', type=int, default=30)
parser.add_argument('--latent_size', type=int, default=128)
parser.add_argument('--epochs', type=int, default=200)

args = parser.parse_args()

args.retroname = args.romartifact.split(':')[0]

# wandb setup, execute first to log into wandb
run = wandb.init(
    project="turbocharger",
    config=args,
    job_type='training')

# get the rom and put it into retro's dir
rom = run.use_artifact(args.romartifact)
rom_dir = rom.download()

retro_location = retro.__file__
retro_path = pathlib.Path(retro.__file__).parent
# print(retro_path)

# Move the rom and associated metadata jsons to the right paths
shutil.copyfile(rom_dir + "/rom.sfc", retro_path/"data/stable"/args.retroname/"rom.sfc")
shutil.copyfile(rom_dir + "/data.json", retro_path/"data/stable"/args.retroname/"data.json")
shutil.copyfile(rom_dir + "/metadata.json", retro_path/"data/stable"/args.retroname/"metadata.json")

# Logging lists for tiles
tiles_touched = dict()
buttons_pressed = list()
unique_tiles_ot = list()

# Now get the human gameplay data (which has the bk2 replay)
# SuperMarioWorld-Snes-human:v2

datasets = dict()

for (split, artifact) in {"train": args.humantrainartifact,"test": args.humantestartifact}.items():
    movie = run.use_artifact(artifact)
    movie_dir = movie.download()

    runs = dict()

    shutil.copyfile(movie_dir + "/" + args.retroname + "-boot-000000.bk2", "./" + args.retroname + "-boot-000000.bk2")
    movies = glob.glob(f"{movie_dir}/*.bk2")
    for m in movies:
        print(m)
        movie = retro.Movie(m)
        movie.step()

        env = retro.make(
            game=movie.get_game(),
            state=None,
            # bk2s can contain any button presses, so allow everything
            use_restricted_actions=retro.Actions.ALL,
            players=movie.players,
        )
        env.initial_state = movie.get_state()
        env.reset()

        steps=0
        rams = []
        buttons = []
        screenshots = []
        stats = []

        while movie.step():
            keys = []
            # Get all of the keys at this frame and load it into the array. This is the button press combination that we will feed into the emulator
            for p in range(movie.players):
                for i in range(env.num_buttons):
                    keys.append(movie.get_key(i, p))
            steps+=1
            print(f"step: {steps}  |  keys: {keys}")
            obs, rew, done, _info = env.step(keys)
            if(steps % args.cap_freq == 0):
                screenshots.append(env.render(mode='rgb_array'))
                buttons.append(np.array(keys))
                rams.append(np.array(env.get_ram()[0:8192]))
                stats.append(_info)
            # Test to see if env is accepting button presses correctly
            # env.render()
            
        env = None
        print(f"Number of steps done: {steps}")
        runs[m] = dict(rams=np.array(rams), buttons=np.array(buttons), screenshots=np.array(screenshots))
    datasets[split] = runs
#     with open(f"{split}.pickle", "wb") as f:
#         pickle.dump(runs, f)

train_data = datasets["train"]
test_data = datasets["test"]

# For testing
exit()

# Begin training memmem2buttons
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

# Get the shapes of the rams and buttons
first_run_key = list(train_data.keys())[0]
mem_shape = train_data[first_run_key]['rams'].shape[1]
buttons_shape = train_data[first_run_key]['buttons'].shape[1] 

# embed_mem model
latent_size = 128

# Single linear layer, no activation, with l2 regularization
ram_input_layer = tf.keras.layers.Input(shape=(mem_shape))
x = ram_input_layer
embedding = tf.keras.layers.Dense(
    latent_size, 
    kernel_regularizer=None, 
    kernel_initializer='zeros', 
    name="mem_embedding_layer", 
    use_bias=False)(x)

embed_mem_model = tf.keras.Model(ram_input_layer, embedding)

embed_mem_model.compile()
embed_mem_model.summary()

# Button Predict model

current_input = tf.keras.layers.Input(shape=(latent_size))
goal_input = tf.keras.layers.Input(shape=(latent_size))

x = tf.keras.layers.Concatenate()([current_input, goal_input]) # shut down inputs
# x = current_input
for i in range(3):
    x = tf.keras.layers.Dense(
        256, 
        activation='tanh', 
        kernel_regularizer='l2')(x)
buttons_layers = tf.keras.layers.Dense(12, 
    activation='sigmoid')(x)
buttons_model = tf.keras.Model([current_input, goal_input], buttons_layers)

buttons_model.compile()
buttons_model.summary()

# memmem2buttons model
print(mem_shape)
current_mem_inputs = tf.keras.layers.Input(shape=(mem_shape))
goal_mem_inputs = tf.keras.layers.Input(shape=(mem_shape))

current_latent = embed_mem_model(current_mem_inputs)
goal_latent = embed_mem_model(goal_mem_inputs)

predict_buttons = buttons_model([current_latent, goal_latent])
memmem2buttons = tf.keras.Model([current_mem_inputs, goal_mem_inputs], predict_buttons)

memmem2buttons.compile(loss="binary_crossentropy", metrics=["binary_accuracy"])
memmem2buttons.summary()

def augment_dataset(dataset):
    inputs = list()
    outputs = list()
    for k in dataset.keys():
        rams = dataset[k]['rams']
        buttons = dataset[k]['buttons']
        num_samples = 20
#         This is currently skipping ahead 10 frames, which is a bit short
        max_skip = 10 
        max_t = len(rams)
        for s in range(num_samples):
            shift = np.minimum(max_t-1, np.arange(max_t) + np.random.randint(0, max_skip, size=max_t))
            inputs.append(np.stack([rams, rams[shift]], axis=0))
            outputs.append(buttons)

        inputs = np.concatenate(inputs, 1)
        outputs = np.concatenate(outputs, 0)
    return inputs, outputs

train_inputs, train_outputs = augment_dataset(train_data)
test_inputs, test_outputs = augment_dataset(test_data)

# Fit?
X = [train_inputs[0], train_inputs[1]]
y = train_outputs
result = memmem2buttons.fit(X, 
                             y, 
                             validation_data=([test_inputs[0], test_inputs[1]], test_outputs),
                             epochs=100, 
                             batch_size=args.epochs, 
                             verbose=1, 
                             callbacks=[WandbCallback(
#                                                       save_model=True, 
#                                                       log_weights=True, 
#                                                       log_gradients=True, 
#                                                       training_data = (X,y)
                                                     )
                                       ])

# Train and test forwards and backwards. 
memmem2buttons.evaluate([test_inputs[0], test_inputs[1]], test_outputs)
memmem2buttons.evaluate([test_inputs[0], test_inputs[0]], test_outputs)
memmem2buttons.evaluate([test_inputs[0], test_inputs[1]], test_outputs[::-1])

memmem2buttons.evaluate([train_inputs[0], train_inputs[1]], train_outputs)
memmem2buttons.evaluate([train_inputs[0], train_inputs[0]], train_outputs)
memmem2buttons.evaluate([train_inputs[0], train_inputs[1]], train_outputs[::-1])

buttons_model.save("./buttons_model")
embed_mem_model.save("./embed_mem_model")

artifact = wandb.Artifact('models', type='model')
artifact.add_dir('buttons_model', name='buttons_model')
artifact.add_dir('embed_mem_model', name='embed_mem_model')
run.log_artifact(artifact)