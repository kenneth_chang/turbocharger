import retro
import argparse
import wandb
import pathlib
import numpy as np
import imageio
import pickle
import os
import shutil
import glob
import tensorflow as tf
from wandb.keras import WandbCallback

parser = argparse.ArgumentParser(description="Training arguments")
parser.add_argument('--romartifact', required=True)
parser.add_argument('--humantrainartifact', required=True)
# parser.add_argument('--humantestartifact', required=True)
parser.add_argument('--cap_freq', type=int, default=30)
parser.add_argument('--latent_size', type=int, default=128)
parser.add_argument('--epochs', type=int, default=200)

args = parser.parse_args()

args.retroname = args.romartifact.split(':')[0]

# wandb setup, execute first to log into wandb
run = wandb.init(
    project="turbocharger",
    config=args,
    job_type='training')

# get the rom and put it into retro's dir
rom = run.use_artifact(args.romartifact)
rom_dir = rom.download()

retro_location = retro.__file__
retro_path = pathlib.Path(retro.__file__).parent
# print(retro_path)

# Move the rom and associated metadata jsons to the right paths
shutil.copyfile(rom_dir + "/rom.sfc", retro_path/"data/stable"/args.retroname/"rom.sfc")
shutil.copyfile(rom_dir + "/data.json", retro_path/"data/stable"/args.retroname/"data.json")
shutil.copyfile(rom_dir + "/metadata.json", retro_path/"data/stable"/args.retroname/"metadata.json")

# Logging lists for tiles
tiles_touched = dict()
buttons_pressed = list()

# Now get the human gameplay data (which has the bk2 replay)
# SuperMarioWorld-Snes-human:v2

datasets = dict()

# ===========================================
# Ken TODO: Render this visibly to verify that the BK2 is played back correctly. 

for (split, artifact) in {"train": args.humantrainartifact,"test": args.humantestartifact}.items():
    movie = run.use_artifact(artifact)
    movie_dir = movie.download()

    runs = dict()

    shutil.copyfile(movie_dir + "/" + args.retroname + "-boot-000000.bk2", "./" + args.retroname + "-boot-000000.bk2")
    movies = glob.glob(f"{movie_dir}/*.bk2")
    for m in movies:
        print(m)
        movie = retro.Movie(m)
        movie.step()

        env = retro.make(
            game=movie.get_game(),
            state=None,
            # bk2s can contain any button presses, so allow everything
            use_restricted_actions=retro.Actions.ALL,
            players=movie.players,
        )
        env.initial_state = movie.get_state()
        env.reset()

        steps=0
        rams = []
        buttons = []
        screenshots = []
        stats = []

        while movie.step():
            keys = []
            # Get all of the keys at this frame and load it into the array. This is the button press combination that we will feed into the emulator
            for p in range(movie.players):
                for i in range(env.num_buttons):
                    keys.append(movie.get_key(i, p))
            steps+=1
            # print(f"step: {steps}")
            obs, rew, done, _info = env.step(keys)
            if(steps % args.cap_freq == 0):
                screenshots.append(env.render(mode='rgb_array'))
                buttons.append(np.array(keys))
                rams.append(np.array(env.get_ram()[0:8192]))
                stats.append(_info)
            
        env = None
        print(f"Number of steps done: {steps}")
        runs[m] = dict(rams=np.array(rams), buttons=np.array(buttons), screenshots=np.array(screenshots))
    datasets[split] = runs
#     with open(f"{split}.pickle", "wb") as f:
#         pickle.dump(runs, f)

with open("stats.pickle", "wb") as f:
    pickle.dump(stats, f)
with open("buttons.pickle", "wb") as f:
    pickle.dump(buttons, f)
exit()

artifact = wandb.Artifact('results', type='model_free')
artifact.add_dir('buttons_model', name='buttons_model')
artifact.add_dir('embed_mem_model', name='embed_mem_model')
run.log_artifact(artifact)