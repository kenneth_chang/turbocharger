import retro
import argparse
import wandb
import pathlib
import numpy as np
import imageio
import pickle
import os
import shutil

parser = argparse.ArgumentParser(description="Human Play arguments")
parser.add_argument('--romartifact', required=True)
parser.add_argument('--cap_freq', default=30)

args = parser.parse_args()
if not args.movie or (".bk2" not in args.movie):
    print("No valid movie selected, exiting")
    exit()

args.retroname = args.romartifact.split(':')[0]

# wandb setup, execute first to log into wandb
run = wandb.init(
    project="turbocharger",
    config=args,
    job_type='data-gather')

# get the rom and put it into retro's dir
rom = run.use_artifact(args.romartifact)
rom_dir = rom.download()

retro_location = retro.__file__
retro_path = pathlib.Path(retro.__file__).parent
print(retro_path)

# Move the rom and associated metadata jsons to the right paths
shutil.copyfile(rom_dir + "/rom.sfc", retro_path/"data/stable"/args.retroname/"rom.sfc")
shutil.copyfile(rom_dir + "/data.json", retro_path/"data/stable"/args.retroname/"data.json")
shutil.copyfile(rom_dir + "/metadata.json", retro_path/"data/stable"/args.retroname/"metadata.json")

# Logging lists for tiles
tiles_touched = dict()
buttons_pressed = list()

# Now get the human gameplay data (which has the bk2 replay)
movie = run.use_artifact(args.retroname + "-human")
movie_dir = movie.download()
# Load the movie
movie = retro.Movie(movie_dir + args.retroname + "--000000.bk2")
movie.step()

env = retro.make(
    game=movie.get_game(),
    state=None,
    # bk2s can contain any button presses, so allow everything
    use_restricted_actions=retro.Actions.ALL,
    players=movie.players,
)
env.initial_state = movie.get_state()
env.reset()

steps=0

rams = []
buttons = []

while movie.step():
    keys = []
    # Get all of the keys at this frame and load it into the array. This is the button press combination that we will feed into the emulator
    for p in range(movie.players):
        for i in range(env.num_buttons):
            keys.append(movie.get_key(i, p))
    steps+=1
    # print(f"step: {steps}")

    if(steps % args.cap_freq == 0):
        # Write out a screencap, as well as a bin file of the buttons pressed here
        # This also guarantees that the button presses here matches the screenshot taken
        imageio.imwrite(os.path.join(result_dir_path + "/Screenshots", f"BK2_Screencap_{steps}.png"), env.render(mode='rgb_array'))
        buttons.append(np.array(keys))
        # with open(os.path.join(result_dir_path + "/Buttons", f"BK2_buttons_{steps}.bin"), 'wb') as f:
        #     # np.save(f, keys)
        #     np.array(keys).tofile(f)

    if(steps % args.cap_freq == 0):
        ramcap = env.get_ram()[RAM_start:RAM_end]
        # print("SNES RAM snapped: " + str(snap_counter))
        rams_temp = np.array(ramcap)
        rams.append(rams_temp)
        # with open(os.path.join(result_dir_path + "/RAM", f"BK2_ramcap_{steps}.bin"), 'wb') as f:
        #     # np.save(f, rams_temp)
        #     rams_temp.tofile(f)

    if(steps % args.cap_freq == 0):
        # imageio.imwrite(os.path.join(result_dir_path, filename), self._env.get_ram()[RAM_start:RAM_end])
        state = env.em.get_state()
        state_path = os.path.join(result_dir_path + "/States", f"BK2_gameplay_{steps}.state")
        with open(state_path, 'wb') as f:
            pickle.dump(state, f,  pickle.HIGHEST_PROTOCOL)
    env.step(keys)

print("Saving buttons and rams")

with open(os.path.join(result_dir_path + "/RAM", f"BK2_rams.npy"), 'wb') as f:
    rams_array = np.array(rams)
    print(f"rams_array shape: {rams_array.shape}")
    rams_array.tofile(f)

with open(os.path.join(result_dir_path + "/Buttons", f"BK2_buttons.npy"), 'wb') as f:
    buttons_array = np.array(buttons)
    print(f"buttons_array shape: {buttons_array.shape}")
    buttons_array.tofile(f)