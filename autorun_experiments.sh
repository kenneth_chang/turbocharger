#!/bin/bash


# # Set vars for executing experiments for SMW Vanilla Behavior Cloning (no rrt)
# MODELARTIFACT="models:v7"
# ROMARTIFACT="SuperMarioWorld-Snes:v9"
# GAMETAG="SMW"
# EXPERIMENTTAG="BEHAVIORCLONE_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
#     python3 behavior_clone.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED --burnin_steps=10
# done

# # Set vars for executing experiments for SMW Vanilla RRT Goal Aware
# MODELARTIFACT="models:v7"
# ROMARTIFACT="SuperMarioWorld-Snes:v9"
# GAMETAG="SMW"
# EXPERIMENTTAG="GoalAware_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
#     python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED
# done

# # Set vars for executing experiments for SMW Vanilla RRT Goal Oblivious
# MODELARTIFACT="models:v7"
# ROMARTIFACT="SuperMarioWorld-Snes:v9"
# GAMETAG="SMW"
# EXPERIMENTTAG="GoalObl_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
#     python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
# done

# # Set vars for executing experiments for SMW Vanilla RRT Random
# MODELARTIFACT="models:v7"
# ROMARTIFACT="SuperMarioWorld-Snes:v9"
# GAMETAG="SMW"
# EXPERIMENTTAG="RRTRandom_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
#     python3 rrt_play.py --romartifact $ROMARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
# done

# # Set vars for executing experiments for SMW Vanilla RRT Random
# MODELARTIFACT="models:v7"
# ROMARTIFACT="SuperMarioWorld-Snes:v9"
# GAMETAG="SMW"
# EXPERIMENTTAG="UniformRandom_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
#     python3 rrt_play.py --romartifact $ROMARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
# done

# ============================= BANZAI 1 =============================

# # Set vars for executing experiments for SMW Vanilla Behavior Cloning (no rrt)
# MODELARTIFACT="models:v7"
# ROMARTIFACT="SuperMarioWorld-Snes:v12"
# GAMETAG="BANZAI_1"
# EXPERIMENTTAG="BEHAVIORCLONE_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
# #     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
#     python3 behavior_clone.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED --burnin_steps=10
# done

# # Set vars for executing experiments for SMW Vanilla RRT Goal Aware
# MODELARTIFACT="models:v7"
# EXPERIMENTTAG="GoalAware_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
# #     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
#     python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED
# done

# # Set vars for executing experiments for SMW Vanilla RRT Goal Oblivious
# MODELARTIFACT="models:v7"
# EXPERIMENTTAG="GoalObl_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
# #     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
#     python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
# done

# # Set vars for executing experiments for SMW Vanilla RRT Random
# MODELARTIFACT="models:v7"
# EXPERIMENTTAG="RRTRandom_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
# #     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
#     python3 rrt_play.py --romartifact $ROMARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
# done

# # Set vars for executing experiments for SMW Vanilla RRT Random
# MODELARTIFACT="models:v7"
# EXPERIMENTTAG="UniformRandom_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
# #     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
#     python3 rrt_play.py --romartifact $ROMARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
# done

# ============================= BANZAI 2 =============================

# # Set vars for executing experiments for SMW Vanilla Behavior Cloning (no rrt)
# MODELARTIFACT="models:v7"
# ROMARTIFACT="SuperMarioWorld-Snes:v13"
# GAMETAG="BANZAI_2"
# EXPERIMENTTAG="BEHAVIORCLONE_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
# #     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
#     python3 behavior_clone.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED --burnin_steps=10
# done

# # Set vars for executing experiments for SMW Vanilla RRT Goal Aware
# EXPERIMENTTAG="GoalAware_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
# #     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
#     python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED
# done

# # Set vars for executing experiments for SMW Vanilla RRT Goal Oblivious
# EXPERIMENTTAG="GoalObl_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
# #     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
#     python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
# done

# # Set vars for executing experiments for SMW Vanilla RRT Random
# EXPERIMENTTAG="RRTRandom_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
# #     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
#     python3 rrt_play.py --romartifact $ROMARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
# done

# # Set vars for executing experiments for SMW Vanilla RRT Random
# EXPERIMENTTAG="UniformRandom_"

# for i in {0..3}
# do
#     echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#     SEED=($RANDOM % 10)
# #     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
#     python3 rrt_play.py --romartifact $ROMARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
# done

# ============================= TRIVIAL =============================

# Set vars for executing experiments for SMW trivial Behavior Cloning (no rrt)
MODELARTIFACT="models:v7"
ROMARTIFACT="SuperMarioWorld-Snes:v14"
GAMETAG="TRIVIAL"
EXPERIMENTTAG="BEHAVIORCLONE_"

for i in {0..3}
do
    echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    SEED=($RANDOM % 10)
#     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
    python3 behavior_clone.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED --burnin_steps=10
done

# Set vars for executing experiments for SMW Vanilla trivial
MODELARTIFACT="models:v7"
EXPERIMENTTAG="GoalAware_"

for i in {0..3}
do
    echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    SEED=($RANDOM % 10)
#     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
    python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED
done

# Set vars for executing experiments for SMW trivial RRT Goal Oblivious
MODELARTIFACT="models:v7"
EXPERIMENTTAG="GoalObl_"

for i in {0..3}
do
    echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    SEED=($RANDOM % 10)
#     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
    python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
done

# Set vars for executing experiments for SMW trivial RRT Random
MODELARTIFACT="models:v7"
EXPERIMENTTAG="RRTRandom_"

for i in {0..3}
do
    echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    SEED=($RANDOM % 10)
#     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
    python3 rrt_play.py --romartifact $ROMARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
done

# Set vars for executing experiments for SMW trivial RRT Random
MODELARTIFACT="models:v7"
EXPERIMENTTAG="UniformRandom_"

for i in {0..3}
do
    echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    SEED=($RANDOM % 10)
#     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
    python3 rrt_play.py --romartifact $ROMARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
done

# ============================= KAIZO =============================

# Set vars for executing experiments for SMW iMPOSSIBLE Behavior Cloning (no rrt)
MODELARTIFACT="models:v7"
ROMARTIFACT="SuperMarioWorld-Snes:v15"
GAMETAG="IMPOSSIBLE"
EXPERIMENTTAG="BEHAVIORCLONE_"

for i in {0..3}
do
    echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    SEED=($RANDOM % 10)
#     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
    python3 behavior_clone.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED --burnin_steps=10
done

# Set vars for executing experiments for SMW Vanilla IMPOSSIBLE
MODELARTIFACT="models:v7"
EXPERIMENTTAG="GoalAware_"

for i in {0..3}
do
    echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    SEED=($RANDOM % 10)
#     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
    python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED
done

# Set vars for executing experiments for SMW IMPOSSIBLE RRT Goal Oblivious
MODELARTIFACT="models:v7"
EXPERIMENTTAG="GoalObl_"

for i in {0..3}
do
    echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    SEED=($RANDOM % 10)
#     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
    python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
done

# Set vars for executing experiments for SMW IMPOSSIBLE RRT Random
MODELARTIFACT="models:v7"
EXPERIMENTTAG="RRTRandom_"

for i in {0..3}
do
    echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    SEED=($RANDOM % 10)
#     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
    python3 rrt_play.py --romartifact $ROMARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
done

# Set vars for executing experiments for SMW IMPOSSIBLE RRT Random
MODELARTIFACT="models:v7"
EXPERIMENTTAG="UniformRandom_"

for i in {0..3}
do
    echo ">>>>>>>>>>>>>>>>>>>>>  EXECUTING $GAMETAG $EXPERIMENTTAG$i  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    SEED=($RANDOM % 10)
#     echo "python3 rrt_play.py --romartifact $ROMARTIFACT --modelartifact $MODELARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals True --seed $SEED"
    python3 rrt_play.py --romartifact $ROMARTIFACT --experiment_tag $EXPERIMENTTAG$i --game_tag $GAMETAG --goals False --seed $SEED
done