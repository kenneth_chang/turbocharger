import retro
import numpy as np
import pickle
import sys 
import os
import random
import argparse
import wandb
import shutil
import pathlib
from tqdm import tqdm
import tensorflow as tf
import gzip
from rrt import RRT
import numpy as np

# Ken get the metadata.json and data.json to be artifacts on wandb

parser = argparse.ArgumentParser(description="rrt_play arguments")
parser.add_argument('--seed', default=0, type=int)
parser.add_argument('--render', default=False, type=bool)


parser.add_argument('--romartifact', type=str, required=True)
parser.add_argument('--modelartifact', type=str, required=False)

parser.add_argument('--metrics_interval', type=int, default=30)
parser.add_argument('--screenshot_interval', type=int, default=600)

parser.add_argument('--burnin_steps', type=int, default=40)
parser.add_argument('--main_steps', type=int, default=1000)
parser.add_argument('--min_steps_per_branch', type=int, default=800)
parser.add_argument('--max_steps_per_branch', type=int, default=1200)
parser.add_argument('--button_dwell', type=int, default=15)
parser.add_argument('--reroll_probability', type=float, default=0.1)
parser.add_argument('--experiment_tag', type=str, required=True)
parser.add_argument('--game_tag', type=str, required=True)
parser.add_argument('--goals', type=bool, default=True)

args = parser.parse_args()
args.retroname = args.romartifact.split(':')[0]

run_tags = list()
run_tags.append(args.game_tag)
run_tags.append(args.experiment_tag)

# wandb setup, execute first to log into wandb
run = wandb.init(
    project="turbocharger",
    config=args,
    job_type='rrt-play',
    tags=run_tags)

# get the rom and put it into retro's dir
rom = run.use_artifact(args.romartifact)
rom_dir = rom.download()
retro_location = retro.__file__
retro_path = pathlib.Path(retro.__file__).parent

# Move the rom and associated metadata jsons to the right paths
shutil.copyfile(rom_dir + "/rom.sfc", retro_path/"data/stable"/args.retroname/"rom.sfc")
shutil.copyfile(rom_dir + "/data.json", retro_path/"data/stable"/args.retroname/"data.json")
shutil.copyfile(rom_dir + "/metadata.json", retro_path/"data/stable"/args.retroname/"metadata.json")

# Logging lists for tiles
tiles_touched = dict()
buttons_pressed = list()
unique_tiles_ot = list()

np.random.seed(args.seed)
tf.random.set_seed(args.seed)

if args.modelartifact is None:
    mem_model = tf.keras.Sequential([
        tf.keras.Input(shape=(8192,)),
        tf.keras.layers.Dense(128),
    ])
    
    current_vec_input = tf.keras.layers.Input(mem_model.output.shape[1:])
    goal_vec_input = tf.keras.layers.Input(mem_model.output.shape[1:])
    combined_vec = tf.keras.layers.Concatenate()([current_vec_input, goal_vec_input])
    buttons_out = tf.keras.layers.Dense(12, activation='sigmoid', kernel_initializer='zeros', bias_initializer='zeros')(combined_vec)
    buttons_model = tf.keras.Model([current_vec_input, goal_vec_input], buttons_out)
else: 
    print("Loading models from artifacts")
    models = run.use_artifact(args.modelartifact)
    models_dir = models.download()
    mem_model = tf.keras.models.load_model(f"{models_dir}/embed_mem_model")
    buttons_model = tf.keras.models.load_model(f"{models_dir}/buttons_model")
    
env = retro.make(game=args.retroname)
env.reset()
            
def getActionTowardsGoal(goal_vec):
    current_vec = getCurrentVec()
#     print(f"current_vec shape: {current_vec.shape}")
    if goal_vec is None:
        goal_vec = current_vec
    buttons = buttons_model.predict([current_vec[None], goal_vec[None]])
    return buttons

def getCurrentVec():
    current_ram = env.get_ram()[0:8192]
    current_ram = np.reshape(np.array(current_ram), (1,8192))
#     print(f"current_ram shape: {current_ram.shape}")
    current_vec = mem_model.predict(current_ram)
    return current_vec[0]

step_counter = 0

# Instead of returning up to num_keys buttons to press, select up to max_buttons to press down
# starting from the highest probabilty value
def chooseButtonCombination(keys, max_buttons=2):
    # 0 is valid, 1 is invalid in np masked arrays
    mask = np.zeros(len(keys))
    
    
def playWithButtons(probabilities):
    
    # Valid levels to explore, if not in valid level spam A
    last_level = -1
    levels = [89, 50, 202, 147]
    press_A = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    press_None = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    spam_counter = 0
    
    pressed_keys = []
    keys = [1 if random.random() < p else 0 for p in probabilities[0]]
    # keys = chooseButtonCombination(probabilities, 2)
    # dwell = 0
    for i in range(random.randrange(args.min_steps_per_branch, args.max_steps_per_branch)):
        # press A up and down. can't just hold A
        if last_level not in levels and spam_counter < 20:
            keys = env.action_space.sample()
            spam_counter += 1
        elif last_level not in levels and spam_counter < 40:
            keys = press_None
            spam_counter += 1
            if spam_counter == 40:
                spam_counter = 0
        else: 
            if(random.random() < args.reroll_probability):
                # R | L | X | A | Right | Left | Y | B | Select | Start | Up | Down |
                keys = [1 if random.random() < p else 0 for p in probabilities[0]]
                # keys = chooseButtonCombination(probabilities, 2)
                # Suppress any pause game button presses
                keys[8] = 0
                keys[9] = 0
        pressed_keys.append(keys)
        # print(f"keys to press: {keys}")
        obs, rew, done, _info = env.step(keys)
        if _info['2player'] == 1:
            print("Reset env due to 2player")
            env.reset()
        if args.render:
            env.render()
        global step_counter
        step_counter += 1
        last_level = _info['level']
        tile = (_info['level'], _info['x_position']//32, _info['y_position']//32)
        tiles_touched[tile] = 1
        unique_tiles_ot.append(len(tiles_touched))
        if step_counter % args.metrics_interval == 0:
            wandb.log(_info, step=step_counter)
            wandb.log({'tiles_touched': len(tiles_touched)}, step=step_counter)
        if step_counter % args.screenshot_interval == 0:
            image_array = env.render(mode='rgb_array')
            wandb.log({"screenshots":[wandb.Image(image_array)]}, step=step_counter)
    return pressed_keys

def explore():

    
    tree = RRT()
    uniform_random = np.array([[0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5]])
    random_breakout_steps = 50
    
    print(">>> Burnin steps")
    for i in tqdm(range(args.burnin_steps)): # fill in a few random states by hitting random buttons
        probabilities = getActionTowardsGoal(None)
        playWithButtons(probabilities)
        tree.create_node(getCurrentVec(), env.em.get_state())

    print(">>> Main steps")
    # Every 50 main steps, run 10 steps of random buttons
    breakout_counter = 0
    for i in tqdm(range(args.main_steps)):
        goal_vec = tree.sample_goal()
        closest_node = tree.find_closest_node(goal_vec)
        closest_vec, closest_state = closest_node

        env.em.set_state(closest_state)

        if breakout_counter < 50:
            # print("Goal mode")
            if args.goals:
                probabilities = getActionTowardsGoal(goal_vec) # play towards that goal for X frames
            else:
                probabilities = getActionTowardsGoal(None) # play towards NO goal for X frames
        else: 
            # print("Breakout mode")
            probabilities = uniform_random

        actions = playWithButtons(probabilities)

        next_node = tree.create_node(getCurrentVec(), env.em.get_state())
        tree.add_edge(closest_node, actions, next_node)
        breakout_counter += 1
        # Reset breakout counter if it hits 60
        if breakout_counter == 60:
            env.reset()
            breakout_counter = 0
    return tree

exploration_tree = explore()
print("Exploration done!")
artifact = wandb.Artifact(args.retroname + "-rrt_play", type='rrt-data')
with open("rrt_tiles_ot.npy", "wb") as f:
    np.save(f, np.array(unique_tiles_ot))
with artifact.new_file("tree.pickle.gz", "wb") as f:
    compressed_tree = gzip.compress(pickle.dumps(exploration_tree)) 
    f.write(compressed_tree)
artifact.add_file('rrt_tiles_ot.npy', 'rrt_tiles_ot.npy')
wandb.log_artifact(artifact)
