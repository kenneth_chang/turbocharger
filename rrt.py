import numpy as np

class RRT(object):
    def __init__(self):
        self.nodes = set()
        self.edges = set()

    def add_edge(self, source, action, target):
        self.edges.add((source, np.packbits(np.array(action), axis=1).tobytes(), target))

    def create_node(self, embedding, state):
        n = (tuple(embedding), state)
        self.nodes.add(n)
        return n

    def find_closest_node(self, goal):
        return min(self.nodes, key=lambda n: ((n[0] - goal)**2).sum())

    def sample_goal(self):
        embeddings = []
        for n in self.nodes:
            embeddings.append(n[0])
            embeddings = np.array(embeddings)
            mean, stddev = embeddings.mean(0), embeddings.std(0)
            return np.random.normal(mean, stddev*8)
    
#     Unpacks the action from add_edge
    def unpack_action(self, action):
        return np.unpackbits(np.frombuffer(action, dtype='uint8').reshape((-1, 2)), axis=-1)[:,:12]