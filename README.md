# human_play.py command line arguments

--romartifact: Name of the rom artifact (in retro naming format) of the rom you wish to play. You must also include a version number. 

Example: python human_play.py --romartifact SuperMarioWorld-Snes:v0

# rrt_play.py 

--seed: Random seed to start
--render: Show gameplay in a GUI
--romartifact: Name of the rom artifact (in retro naming format) of the rom you wish to play. You must also include a version number. 
--embeddingartifact: Name of the embedding artifact
--actionartifact: Name of the action artifact
--metrics_interval: How often to collect metrics 

Example: python rrt_play.py --romartifact SuperMarioWorld-Snes:v0

# replay_and_record.py

Example: python replay_and_record --romartifact SuperMarioWorld-Snes:v0 